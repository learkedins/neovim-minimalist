local modules = {
	"core.options",
	"core.mappings",
	"custom",
	"core.colors"
}

for _, module in ipairs(modules) do
	local status_ok, error = pcall(require, module)
	if not status_ok then
		return
	end
end
