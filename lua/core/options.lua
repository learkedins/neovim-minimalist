local options = {
  shiftwidth = 2,
  number = true,
  signcolumn = "yes",
  showtabline = 2,
  undofile = true,
  swapfile = false,
  smartcase = true,
  ignorecase = true,
  mouse = "a",
  pumheight = 10,
  completeopt = { "menuone", "noselect" },
  conceallevel = 0,
  backup = false,
  softtabstop = 2,
  tabstop = 2,
  wrap = false,
  fileencoding = "utf-8",
  cmdheight = 2,
  writebackup = false,
  scrolloff = 4,
  sidescrolloff = 4,
  timeoutlen = 100,
  clipboard = "unnamedplus",
  cursorline = true,
  hidden = true,
  updatetime = 300,
  showmode = false,
  numberwidth = 4,
  expandtab = true
}

for k,v in pairs(options) do
  vim.opt[k] = v
end

local custom_options = require('custom').options
for k,v in pairs(custom_options) do
  vim.opt[k] = v
end
