local M = {}

M.options = {
  termguicolors = true,
}

M.appearance = {
  theme = default,
}

M.mappings = function() 
  require('custom.mappings')
end

return M
