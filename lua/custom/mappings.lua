local map = vim.api.nvim_set_keymap
local opt = { noremap = true, silent = true }

-- Guide to mappings
-- map(mode, keybinds, command, opt)
--
-- MODE
-- i for insert
-- v for visual
-- n for normal
-- x for visual block
-- c for command mode
-- t for term mode

map("i", "jk", "<ESC>", opt)
